import java.util.Arrays;
import java.util.Scanner;

public class MainArray {
    public static void main(String[] args) {
        int[] v = readArray();

//        System.out.println("Given array is " + Arrays.toString(v));
//        System.out.println("Average of the elements is " + makeAverage(v));
//        printMaxAndMin(v);
//        printEvenAndOdd(v);
//        System.out.println("Reversed array is :\n" + Arrays.toString(reversedArray(v)));
        //printByPosition(v);
        printDuplicate(v);
        //bubbleSort(v);
    }

    public static int[] readArray() {
        int len;
        int[] a;
        Scanner myScan = new Scanner(System.in);

        System.out.println("Give the length of array: ");
        len = myScan.nextInt();
        a = new int[len];

        System.out.println("Give elements: ");
        for(int i = 0; i < len; i++) {
            a[i] = myScan.nextInt();
        }

        return a;
    }

    public static double makeAverage(int[] a) {
        double sum = 0;

        for(int element : a) {
            sum += element;
        }

        return sum/a.length;
    }

    public static void printMaxAndMin(int[] a) {
        int min = a[0];
        int max = a[0];

        for(int element : a) {
            if(element > max) {
                max = element;
            }

            if(element < min) {
                min = element;
            }
        }

        System.out.println("Maxim is " + max + "\nMinim is " + min);
    }

    public static void printEvenAndOdd(int[] a) {
        System.out.println("Odd numbers are: ");
        for(int i : a) {
            if(Math.abs(i) % 2 == 1) {
                System.out.print(i + " ");
            }
        }

        System.out.println("\nEven numbers are: ");
        for(int i : a) {
            if(Math.abs(i) % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    public static int[] reversedArray(int[] a) {
        int[] b = new int[a.length];

        for(int i = (a.length-1); i >=0; i--) {
            b[(a.length-1) - i] = a[i];
        }

        return b;
    }

    public static void printByPosition(int[] a) {
        System.out.println("Your array is " + Arrays.toString(a) + "\n");
        System.out.println("Numbers from even positions are: ");
        for(int i = 0; i < a.length; i++) {
            if(i % 2 == 0) {
                System.out.print(a[i] + " ");
            }
        }

        System.out.println("\nNumbers from odd positions are: ");
        for(int i = 0; i < a.length; i++) {
            if(i % 2 != 0 ) {
                System.out.print(a[i] + " ");
            }
        }
    }

    public static void printDuplicate(int[] a) {

        bubbleSort(a);

        System.out.println("Duplicate elements are: ");

        int i = 0;
        int n = a.length;
        int count = 0;
        while(i < (n-1)) {
            if(a[i] == a[i+1]) {
                count++;
                System.out.print(a[i] + " ");
                int j = i + 2;
                while (j < n && a[j] == a[j-1]) {
                    j++;
                }
                i = j;
            } else i++;

        }

        if (count == 0)
            System.out.println("!!!No duplicate elements found!!!");
    }

    public static void bubbleSort(int[] a) {
        boolean isSort = false;
        int aux;

        while(!isSort) {
            isSort = true;

            for(int i = 0; i < (a.length-1); i++) {
                if( a[i] > a[i+1]) {
                    aux = a[i];
                    a[i] = a[i+1];
                    a[i+1] = aux;
                    isSort = false;
                }
            }
        }

        System.out.println("Sorted array is \n" + Arrays.toString(a));
    }
}
